package be.vdab.chatApp.forum;

import be.vdab.chatApp.helpers.JpaHelper;

import java.util.List;

public class ForumRepositoryImpl implements ForumRepository{
    private JpaHelper jpaHelper;


    public ForumRepositoryImpl() {
        this(new JpaHelper());
    }

    public ForumRepositoryImpl(JpaHelper jpaHelper) {
        this.jpaHelper = jpaHelper;
    }

    @Override
    public Forum getForum(long id){
        return jpaHelper.find(Forum.class, id);
    }

    @Override
    public void addForum(Forum... forums){
        jpaHelper.beginTransaction();
        for(Forum forum : forums){
            jpaHelper.save(forum);
        }
        jpaHelper.commitTransaction();
    }

    @Override
    public List<Forum> getAllForum(){
        return jpaHelper.getEntityManager().createQuery("select f from Forum f", Forum.class).getResultList();
    }

    @Override
    public void deleteForum(Forum... forums){
        jpaHelper.beginTransaction();
        for(Forum forum : forums){
            jpaHelper.remove(forum);
        }
        jpaHelper.commitTransaction();
    }


    public void close(){
        jpaHelper.close();
    }
}
