package be.vdab.chatApp.forum;

import java.util.List;

public interface ForumRepository extends AutoCloseable {
    Forum getForum(long id);

    void addForum(Forum... forums);

    List<Forum> getAllForum();

    void deleteForum(Forum... forums);
}
