package be.vdab.chatApp.forum;

import be.vdab.chatApp.message.ForumMessage;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "Fora")
public class Forum {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id")
    private long id;

    @OneToMany(mappedBy = "forum", cascade = CascadeType.REMOVE)
    private List<ForumMessage> messages = new ArrayList<>();

    @Column(name = "Title")
    private String title;

    @Column(name = "IsClosed")
    private boolean isClosed;

    public Forum() {
    }

    public Forum(List<ForumMessage> messages, String title) {
        this.messages = messages;
        this.title = title;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public List<ForumMessage> getMessages() {
        return messages;
    }

    public void setMessages(List<ForumMessage> messages) {
        this.messages = messages;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "Forum{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", isClosed=" + isClosed +
                '}';
    }
}
