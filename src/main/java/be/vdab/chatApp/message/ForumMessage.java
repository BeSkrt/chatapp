package be.vdab.chatApp.message;

import be.vdab.chatApp.forum.Forum;
import be.vdab.chatApp.utility.KeyBoardHelper;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "ForumMessages")
public class ForumMessage extends Message {
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    @Column(name = "Id")
//    private long id;

    @ManyToOne
    @JoinColumn(name = "ForumId")
    private Forum forum;

    public ForumMessage() {
    }

    public ForumMessage(Forum forum) {
        this.forum = forum;
    }

    public Forum getForum() {
        return forum;
    }

    public void setForum(Forum forum) {
        this.forum = forum;
    }

    public void writeMessage() {
        setTimeStamp(LocalDateTime.now());
        System.out.println("Please enter your message:");
        String content = KeyBoardHelper.askForString();
        setContent(content);
    }

    @Override
    public String toString() {
        return "ForumMessage{" +
                "id=" + getId() +
                '}';
    }
}
