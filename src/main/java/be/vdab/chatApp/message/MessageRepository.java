package be.vdab.chatApp.message;

import be.vdab.chatApp.forum.Forum;

import java.util.List;

public interface MessageRepository extends AutoCloseable {
    Message getMessage(long id);

    void addMessage(Message... messages);

    List<Message> getAllMessages();

    void deleteMessage(Message... messages);
}
