package be.vdab.chatApp.message;

import be.vdab.chatApp.user.User;

import javax.persistence.*;

@Entity
@Table(name = "PrivateMessages")
public class PrivateMessage extends Message {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id")
    private long id;

    @ManyToOne
    @JoinColumn(name = "ReceiverId")
    private User receiver;

    public User getReceiver() {
        return receiver;
    }

    public void setReceiver(User receiver) {
        this.receiver = receiver;
    }

    public PrivateMessage() {
    }

    public PrivateMessage(User receiver) {
        this.receiver = receiver;
    }



    @Override
    public String toString() {
        return "PrivateMessage{" +
                "receiver=" + receiver +
                '}';
    }
}
