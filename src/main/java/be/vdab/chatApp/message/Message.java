package be.vdab.chatApp.message;

import be.vdab.chatApp.user.User;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "Messages")
public abstract class Message {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id")
    private long id;
    @Column(name = "TimeStamp")
    private LocalDateTime timeStamp;
    @Column(name = "Content")
    private String content;
    @JoinColumn(name = "UserNameId")
    @ManyToOne
    private User user;

    public Message() {
    }

    public Message(LocalDateTime timeStamp, String content) {
        this.timeStamp = timeStamp;
        this.content = content;
    }

    public long getId() {
        return id;
    }

    public LocalDateTime getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(LocalDateTime timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "Message{" +
                "id=" + id +
                ", timeStamp=" + timeStamp +
                ", content='" + content + '\'' +
                ", user=" + user +
                '}';
    }
}
