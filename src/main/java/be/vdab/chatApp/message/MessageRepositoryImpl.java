package be.vdab.chatApp.message;

import be.vdab.chatApp.helpers.JpaHelper;

import java.util.List;

public class MessageRepositoryImpl implements MessageRepository{
    private JpaHelper jpaHelper;


    public MessageRepositoryImpl() {
        this(new JpaHelper());
    }

    public MessageRepositoryImpl(JpaHelper jpaHelper) {
        this.jpaHelper = jpaHelper;
    }

    @Override
    public Message getMessage(long id) {
        return jpaHelper.find(Message.class, id);
    }

    @Override
    public void addMessage(Message... messages) {
        jpaHelper.beginTransaction();
        for(Message message : messages){
            jpaHelper.save(message);
        }
        jpaHelper.commitTransaction();
    }

    @Override
    public List<Message> getAllMessages() {
        return jpaHelper.getEntityManager().createQuery("select f from Message f", Message.class).getResultList();
    }

    @Override
    public void deleteMessage(Message... messages) {
        jpaHelper.beginTransaction();
        for(Message message : messages){
            jpaHelper.remove(message);
        }
        jpaHelper.commitTransaction();
    }

    @Override
    public void close() {
        jpaHelper.close();
    }
}
