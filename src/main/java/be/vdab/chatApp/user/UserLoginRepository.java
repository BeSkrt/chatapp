package be.vdab.chatApp.user;

import java.time.LocalDate;
import java.util.List;

public interface UserLoginRepository extends AutoCloseable{
    UserLogin getUserLogin(long id);

    void addUserLogin(UserLogin... users);

    UserLogin getUserLoginByQuery(String searchString, String searchString2);

    UserLogin getUserLoginByQuery(String searchString, String searchString2, String searchString3);

    List<UserLogin> getAllUserLogins();

    void updateUserName(UserLogin userLogin, String newName);

    void updatePassword(UserLogin userLogin, String newPassword);

    void updateEmail(UserLogin userLogin, String newEmail);
}
