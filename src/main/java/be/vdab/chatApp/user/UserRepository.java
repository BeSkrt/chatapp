package be.vdab.chatApp.user;

import be.vdab.chatApp.message.Message;

import java.time.LocalDate;
import java.util.List;

public interface UserRepository extends AutoCloseable{
    public User getUserByQuery(String searchString);

    User getUserByQuery(String searchString, String searchString2, String searchString3);

    User getUser(String userName);

    User getUser(long id);

    void addUser(User... users);

    void updateUserName(User user, String newName);

    void updateBirthday(User user, UserLogin userLogin, LocalDate newBirthday);

    List<User> getAllUser();

    void deleteMessage(User... users);

    void updateBio(User user, UserLogin userLogin, String askForBio);
}
