package be.vdab.chatApp.user;

import be.vdab.chatApp.helpers.JpaHelper;

import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.List;

public class UserLoginRepositoryImpl implements UserLoginRepository{
    private JpaHelper jpaHelper;

    public UserLoginRepositoryImpl() {
        this(new JpaHelper());
    }

    public UserLoginRepositoryImpl(JpaHelper jpaHelper) {
        this.jpaHelper = jpaHelper;
    }

    @Override
    public UserLogin getUserLogin(long id) {
        return jpaHelper.find(UserLogin.class, id);
    }

    @Override
    public void addUserLogin(UserLogin... userLogins) {
        jpaHelper.beginTransaction();
        for(UserLogin userLogin : userLogins){
            jpaHelper.save(userLogin);
        }
        jpaHelper.commitTransaction();
    }

    @Override
    public UserLogin getUserLoginByQuery(String searchString, String searchString2, String searchString3) {
        TypedQuery<UserLogin> typedQuery = jpaHelper.findByQuery(UserLogin.class,
                "select u from UserLogin u where u.userName = :search and u.password = :search2 and u.email = :search3");
        typedQuery.setParameter("search", searchString);
        typedQuery.setParameter("search2", searchString2);
        typedQuery.setParameter("search3", searchString3);
        return typedQuery.getSingleResult();
    }

    @Override
    public UserLogin getUserLoginByQuery(String searchString, String searchString2) {
        TypedQuery<UserLogin> typedQuery = jpaHelper.findByQuery(UserLogin.class,
                "select u from UserLogin u where u.userName = :search and u.password = :search2");
        typedQuery.setParameter("search", searchString);
        typedQuery.setParameter("search2", searchString2);
        return typedQuery.getSingleResult();
    }

    @Override
    public void updateUserName(UserLogin userLogin, String newName) {
        jpaHelper.beginTransaction();
        Query query = jpaHelper.getEntityManager().createQuery(
                "update UserLogin as u set u.userName = :newUser where u.userName = :oldUser");
        query.setParameter("newUser", newName);
        query.setParameter("oldUser", userLogin.getUserName());
        query.executeUpdate();
        userLogin.setUserName(newName);
        jpaHelper.commitTransaction();
    }

    @Override
    public void updatePassword(UserLogin userLogin, String newPassword) {
        jpaHelper.beginTransaction();
        Query query = jpaHelper.getEntityManager().createQuery(
                "update UserLogin as u set u.password = :newPassword where u.password = :oldPassword"
        );
        query.setParameter("newPassword", newPassword);
        query.setParameter("oldPassword", userLogin.getPassword());
        query.executeUpdate();
        userLogin.setPassword(newPassword);
        jpaHelper.commitTransaction();
    }

    @Override
    public void updateEmail(UserLogin userLogin, String newEmail) {
        jpaHelper.beginTransaction();
        Query query = jpaHelper.getEntityManager().createQuery(
                "update UserLogin as u set u.email = :newEmail where u.email = :oldEmail"
        );
        query.setParameter("newEmail", newEmail);
        query.setParameter("oldEmail", userLogin.getEmail());
        query.executeUpdate();
        userLogin.setEmail(newEmail);
        jpaHelper.commitTransaction();
    }



    @Override
    public List<UserLogin> getAllUserLogins() {
        return jpaHelper.getEntityManager().createQuery("select f from UserLogin f", UserLogin.class).getResultList();
    }

    @Override
    public void close(){
        jpaHelper.close();
    }
}
