package be.vdab.chatApp.user;

import be.vdab.chatApp.friendRequest.FriendRequest;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "Users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "UserId")
    private long id;
    @Column(name = "UserName")
    private String userName;
    @Column(name = "Birthday")
    private LocalDate birthDay;
    @Column(name = "Description")
    private String description;
    @OneToMany(mappedBy = "user1")
    private List<FriendRequest> sentRequests = new ArrayList<>();
    @OneToMany(mappedBy = "user2")
    private List<FriendRequest> receivedRequests = new ArrayList<>();

    public User() {
    }

    public User(String userName) {
        this.userName = userName;
    }

    public User(LocalDate birthDay, String description) {
        this.birthDay = birthDay;
        this.description = description;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public LocalDate getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(LocalDate birthDay) {
        this.birthDay = birthDay;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getId() {
        return id;
    }


    @Override
    public String toString() {
        return "User{" +
                "userName='" + userName + '\'' +
                ", birthDay=" + birthDay +
                ", description='" + description + '\'' +
                ", sentRequests=" + sentRequests +
                ", receivedRequests=" + receivedRequests +
                '}';
    }
}
