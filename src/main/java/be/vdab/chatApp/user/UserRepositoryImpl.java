package be.vdab.chatApp.user;

import be.vdab.chatApp.helpers.JpaHelper;

import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.time.LocalDate;
import java.util.List;

public class UserRepositoryImpl implements UserRepository{
    private JpaHelper jpaHelper;


    public UserRepositoryImpl() {
        this(new JpaHelper());
    }

    public UserRepositoryImpl(JpaHelper jpaHelper) {
        this.jpaHelper = jpaHelper;
    }

    @Override
    public User getUser(long id) {
        return jpaHelper.find(User.class, id);
    }

    @Override
    public User getUser(String userName) {
        return jpaHelper.find(User.class, userName);
    }

    @Override
    public User getUserByQuery(String searchString) {
        TypedQuery<User> typedQuery = jpaHelper.findByQuery(User.class, "select u from User u where u.userName = :search");
        typedQuery.setParameter("search", searchString);
        return typedQuery.getSingleResult();
    }

    @Override
    public User getUserByQuery(String searchString, String searchString2, String searchString3) {
        TypedQuery<User> typedQuery = jpaHelper.findByQuery(User.class,
                "select u from User u where u.userName = :search and u.password = :search2 and u.email = :search3");
        typedQuery.setParameter("search", searchString);
        typedQuery.setParameter("search2", searchString2);
        typedQuery.setParameter("search3", searchString3);
        return typedQuery.getSingleResult();
    }

    @Override
    public void addUser(User... users) {
        jpaHelper.beginTransaction();
        for(User user : users){
            jpaHelper.save(user);
        }
        jpaHelper.commitTransaction();
    }

    @Override
    public void updateUserName(User user, String newName) {
        jpaHelper.beginTransaction();
        Query query = jpaHelper.getEntityManager().createQuery(
                "update User as u set u.userName = :newName where u.userName = :oldName");
        query.setParameter("newName", newName);
        query.setParameter("oldName", user.getUserName());
        query.executeUpdate();
        user.setUserName(newName);
        jpaHelper.commitTransaction();
    }

    @Override
    public void updateBirthday(User user, UserLogin userLogin, LocalDate newBirthday) {
        jpaHelper.beginTransaction();
        Query query = jpaHelper.getEntityManager().createQuery(
                "update User as u set u.birthDay = :newBirthday where u.birthDay = :oldBirthday");
        query.setParameter("newBirthday", newBirthday);
        query.setParameter("oldBirthday", user.getBirthDay());
        query.executeUpdate();
        userLogin.getUser().setBirthDay(newBirthday);
        jpaHelper.commitTransaction();
    }


    @Override
    public void updateBio(User user, UserLogin userLogin, String newDescription) {
        jpaHelper.beginTransaction();
        Query query = jpaHelper.getEntityManager().createQuery(
                "update User as u set u.description = :newDescription where u.description = :oldDescription");
        query.setParameter("newDescription", newDescription);
        query.setParameter("oldDescription", user.getDescription());
        query.executeUpdate();
        userLogin.getUser().setDescription(newDescription);
        jpaHelper.commitTransaction();
    }

    @Override
    public List<User> getAllUser() {
        return jpaHelper.getEntityManager().createQuery("select f from User f", User.class).getResultList();
    }

    @Override
    public void deleteMessage(User... users) {
        jpaHelper.beginTransaction();
        for(User user : users){
            jpaHelper.save(user);
        }
        jpaHelper.commitTransaction();

    }

    @Override
    public void close() {
        jpaHelper.close();
    }
}
