package be.vdab.chatApp.user;

import javax.persistence.*;
import javax.validation.constraints.*;

@Entity
@Table(name = "UserLogins")
public class UserLogin {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id")
    private long id;
    @Email
    @Column(name = "Email")
    private String email;
    @Column(name = "Password")
    //@Size(min = 12, max = 32)
    @Pattern(regexp = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@$!%*?&/])[A-Za-z\\d@$!%*?&]{12,32}$")
    private String password;
    @Size(max = 32)
    @NotBlank
    @Column(name = "UserName")
    private String userName;
    @OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "UserId")
    private User user;

    public UserLogin() {
    }

    public UserLogin(String email, String password, String userName) {
        this.email = email;
        this.password = password;
        this.userName = userName;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Override
    public String toString() {
        return "UserLogin{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", user=" + user +
                '}';
    }
}
