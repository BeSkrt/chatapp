package be.vdab.chatApp.menu;

import be.vdab.chatApp.forum.Forum;
import be.vdab.chatApp.forum.ForumRepository;
import be.vdab.chatApp.forum.ForumRepositoryImpl;
import be.vdab.chatApp.message.ForumMessage;
import be.vdab.chatApp.message.Message;
import be.vdab.chatApp.message.MessageRepository;
import be.vdab.chatApp.message.MessageRepositoryImpl;
import be.vdab.chatApp.user.User;
import be.vdab.chatApp.user.UserLogin;
import be.vdab.chatApp.user.UserLoginRepository;
import be.vdab.chatApp.user.UserLoginRepositoryImpl;
import be.vdab.chatApp.utility.KeyBoardHelper;
import be.vdab.chatApp.utility.UserRegistry;
import net.bytebuddy.description.method.ParameterList;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class Menu {
    private static User user = new User();
    private static UserLogin userLogin = new UserLogin();
    static int error = 0;
    private static int forumChoice;

    public static void startMenu() {

        do {
            MenuHelper.mainMenu();
            try {
                int menuChoice = KeyBoardHelper.askForNumber();
                switch (menuChoice) {
                    case 1:
                        user = UserRegistry.registerUserAndUserLogin();
                        System.out.println("Successfully registered.\n" + user.getUserName());
                        break;
                    case 2:
                        loginMenu();
                        loggedInMenu();
                        break;
                    case 3:
                        System.out.println("See you next time!");
                        System.exit(0);
                        break;
                    default:
                        System.err.println("Please enter a number corresponding with the menu.");
                        break;
                }

            } catch (Exception e) {
                System.err.println("Please enter a number corresponding with the menu.");
            }
        } while (true);
    }

    public static void loginMenu() {
        boolean done = false;

        do {
            MenuHelper.logIn();
            try {
                int menuChoice = KeyBoardHelper.askForNumber();
                switch (menuChoice) {
                    case 1:
                        userLogin = UserRegistry.findUserLogin();
                        System.out.println("Welcome " + userLogin.getUserName() + "!");
                        user = userLogin.getUser();
                        done = true;
                        break;
                    case 2:
                        done = true;
                        break;
                    default:
                        System.err.println("Please enter a number corresponding with the menu.");
                        break;
                }
            } catch (Exception e) {
                System.err.println("Please enter a number corresponding with the menu.");
            }
        } while (!done);

    }

    public static void loggedInMenu() {
        boolean done = false;

        do {
            MenuHelper.loggedIn();
            try {
                int menuChoice = KeyBoardHelper.askForNumber();
                switch (menuChoice) {
                    case 1:
                        forumMenu();
                        done = true;
                        break;
                    case 2:
                        System.out.println("Welcome to the Friends section");
                        done = true;
                        break;
                    case 3:
                        updateMenu();
                        break;
                    case 4:
                        done = true;
                        break;
                    default:
                        System.err.println("Please enter a number corresponding with the menu.");
                        break;
                }
            } catch (Exception e) {
                System.err.println("Please enter a number corresponding with the menu.");
            }
        } while (!done);

    }

    public static void updateMenu() {
        boolean done = false;

        do {
            UserLoginRepository userLoginRepository = new UserLoginRepositoryImpl();
            userLogin = userLoginRepository.getUserLoginByQuery(userLogin.getUserName(),
                    userLogin.getPassword(), userLogin.getEmail());
            MenuHelper.updateDetails();
            try {
                int menuChoice = KeyBoardHelper.askForNumber();
                switch (menuChoice) {
                    case 1:
                        UserRegistry.sendUpdatedName(user, userLogin, KeyBoardHelper.askForNewUserName());
                        break;
                    case 2:
                        UserRegistry.sendUpdatedEmail(userLogin, KeyBoardHelper.askForNewEmail());
                        break;
                    case 3:
                        UserRegistry.sendUpdatedPassword(userLogin, KeyBoardHelper.askForNewPassword());
                        break;
                    case 4:
                        UserRegistry.sendUpdatedBirthday(user, userLogin, KeyBoardHelper.askForBirthday());
                        break;
                    case 5:
                        UserRegistry.sendUpdatedBio(user, userLogin, KeyBoardHelper.askForBio());
                        break;
                    case 6:
                        done = true;
                        break;
                    default:
                        System.err.println("Please enter a number corresponding with the menu.");
                        break;
                }
            } catch (Exception e) {
                System.err.println("Please enter a number corresponding with the menu.");
            }
        } while (!done);

    }

    public static void forumMenu() {
        boolean done = false;
        ForumRepository forumRepository = new ForumRepositoryImpl();
        List<ForumMessage> forumMessages = new ArrayList<>();

        do {
            MenuHelper.forumText();
            try {
                int menuChoice = KeyBoardHelper.askForNumber();
                switch (menuChoice) {
                    case 1:
                        System.out.println("Enter a name for the new Forum");
                        Forum forum = new Forum(forumMessages, KeyBoardHelper.askForString());
                        forumRepository.addForum(forum);
                        break;
                    case 2:
                        List<Forum> allForum = forumRepository.getAllForum();
                        int number = 1;
                        for (Forum forum2 : allForum) {
                            System.out.println(number + ". " + forum2.getTitle());
                            number++;
                        }
                        chooseForum();
                        break;
                    case 3:
                        done = true;
                        break;
                    default:
                        System.err.println("Please enter a number corresponding with the menu.");
                        break;
                }

            } catch (Exception e) {
                System.err.println("Please enter a number corresponding with the menu.");
            }
        } while (!done);
    }

    public static void chooseForum() {
        boolean done = false;
        Forum forum = new Forum();
        ForumRepository forumRepository = new ForumRepositoryImpl();
        do {
            MenuHelper.chooseForumText();
            try {
                int menuChoice = KeyBoardHelper.askForNumber();
                forum.setId(menuChoice);
                forumChoice = menuChoice;
                if (menuChoice == forum.getId()) {
                    System.out.println(forumRepository.getForum(menuChoice).getMessages());
                }
                createForumMessage();
                done = true;
            } catch (Exception e) {
                System.err.println("Please enter a number corresponding with the menu.");
            }
        } while (!done);
    }

    public static void createForumMessage() {
        boolean done = false;
        ForumRepository forumRepository = new ForumRepositoryImpl();
        List<ForumMessage> forumMessages = new ArrayList<>();
        Forum forum = forumRepository.getForum(forumChoice);

        do {
            MenuHelper.createForumMessageText();
            try {
                int menuChoice = KeyBoardHelper.askForNumber();
                forum.setId(forumChoice);
                switch (menuChoice) {
                    case 1:
                        ForumMessage forumMessage = new ForumMessage();
                        MessageRepository messageRepository = new MessageRepositoryImpl();
                        forumMessage.setForum(forum);
                        forumMessage.writeMessage();
                        forum.getMessages().add(forumMessage);
                        messageRepository.addMessage(forumMessage);
                        break;
                    case 2:
                        System.out.println(forum.getTitle());
                        System.out.println("--------------------");
                        forum.getMessages().forEach(System.out::println);
                        break;
                    case 3:
                        done = true;
                        break;
                    default:
                        System.err.println("Please enter a number corresponding with the menu.");
                        break;
                }
            } catch (Exception e) {
                System.err.println("Please enter a number corresponding with the menu.");
            }
        } while (!done);
    }


}
