package be.vdab.chatApp.menu;

public class MenuHelper {

    public static void mainMenu() {
        System.out.println("--------------------");
        System.out.println("Welcome to LeChat app.");
        System.out.println("--------------------");
        System.out.println("What do you want to do?");
        System.out.println("Type in the number that corresponds with your choice.");
        System.out.println("--------------------");
        System.out.println("1. Register");
        System.out.println("2. Login");
        System.out.println("3. Quit application.");
        System.out.println("--------------------");
    }

    public static void logIn() {
        System.out.println("--------------------");
        System.out.println("What do you want to do?");
        System.out.println("Type in the number that corresponds with your choice.");
        System.out.println("--------------------");
        System.out.println("1. Login");
        System.out.println("2. Quit");
        System.out.println("--------------------");
    }

    public static void loggedIn(){
        System.out.println("--------------------");
        System.out.println("What do you want to do?");
        System.out.println("Type in the number that corresponds with your choice.");
        System.out.println("--------------------");
        System.out.println("1. Forums");
        System.out.println("2. Friends");
        System.out.println("3. Update details");
        System.out.println("4. Exit");
        System.out.println("--------------------");
    }

    public static void updateDetails(){
        System.out.println("--------------------");
        System.out.println("What do you want to do?");
        System.out.println("Type in the number that corresponds with your choice.");
        System.out.println("--------------------");
        System.out.println("1. Update Username");
        System.out.println("2. Update Email");
        System.out.println("3. Update Password");
        System.out.println("4. Update Birthday");
        System.out.println("5. Update Bio");
        System.out.println("6. Exit");
        System.out.println("--------------------");
    }

        public static void forumText(){
            System.out.println("--------------------");
            System.out.println("What do you want to do?");
            System.out.println("Type in the number that corresponds with your choice.");
            System.out.println("--------------------");
            System.out.println("1. Create Forum");
            System.out.println("2. Choose Forum");
            System.out.println("3. Exit");
            System.out.println("--------------------");
        }

        public static void chooseForumText(){
            System.out.println("--------------------");
            System.out.println("Enter the number of the forum you want to visit.");
            System.out.println("--------------------");
        }

        public static void createForumMessageText(){
        System.out.println("--------------------");
        System.out.println("1. Add message");
        System.out.println("2. Show messages");
        System.out.println("3. Exit");
        System.out.println("--------------------");
        }
}

