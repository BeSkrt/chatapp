package be.vdab.chatApp.helpers;

import javax.persistence.*;

public class JpaHelper implements AutoCloseable {
    private EntityManagerFactory entityManagerFactory;
    private EntityManager entityManager;
    private EntityTransaction entityTransaction;

    public JpaHelper() {
        this("course");
    }

    public JpaHelper(String persistenceUnit) {
        this(Persistence.createEntityManagerFactory(persistenceUnit));
    }

    public JpaHelper(EntityManagerFactory entityManagerFactory) {
        this.entityManagerFactory = entityManagerFactory;
        this.entityManager = this.entityManagerFactory.createEntityManager();
        this.entityTransaction = entityManager.getTransaction();
    }

    public EntityManager getEntityManager() {
        return entityManager;
    }

    public <T> void save(T object) {
        entityManager.persist(object);
    }

    public <T> T find(Class<T> tClass, Object id) {
        return entityManager.find(tClass, id);
    }

    public <T> TypedQuery<T> findByQuery(Class<T> tClass, String query) {
        return entityManager.createQuery(query, tClass);
    }

    public Query findByQuery(String query) {
        return entityManager.createQuery(query);
    }

    public <T> TypedQuery<T> findByNamedQuery(Class<T> tClass, String name) {
        return entityManager.createNamedQuery(name, tClass);
    }


    public <T> void detach(T object) {
        entityManager.detach(object);
    }

    public <T> void remove(T object) {
        entityManager.remove(object);
    }

    public <T> void merge(T object) {
        entityManager.merge(object);
    }

    public void beginTransaction() {
        entityTransaction.begin();
    }

    public void commitTransaction() {
        entityTransaction.commit();
    }

    @Override
    public void close() {
        entityManager.close();
        entityManagerFactory.close();
    }
}