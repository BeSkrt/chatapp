package be.vdab.chatApp.utility;

import be.vdab.chatApp.user.*;

import java.time.LocalDate;

public class UserRegistry {
    private static UserRepository userRepository = new UserRepositoryImpl();
    private static UserLoginRepository userLoginRepository = new UserLoginRepositoryImpl();

    public static User registerUserAndUserLogin() {
        UserLogin userLogin = new UserLogin(KeyBoardHelper.askForEmail(),
                KeyBoardHelper.askForPassword(), KeyBoardHelper.askForUserName());
        User user = new User();
        userLogin.setUser(user);
        userLogin.getUser().setDescription("This person didn’t add a description yet");
        userLogin.getUser().setBirthDay(LocalDate.of(2000, 1, 1));
        user.setUserName(userLogin.getUserName());

        userLoginRepository.addUserLogin(userLogin);
        return user;
    }

    public static UserLogin findUserLogin() {
        UserLogin userLogin = null;
        boolean done = false;

        do {
            String askForName = KeyBoardHelper.askForUserName();
            String askForPassword = KeyBoardHelper.askForPassword();
            try {
                 userLogin = userLoginRepository.getUserLoginByQuery(askForName, askForPassword);
                 done = false;
            } catch (Exception e) {
                done = true;
                System.err.println("Could not find user in database.");
                System.err.println("Please enter a valid username and password.");
            }
        } while(done);
        return userLogin;
    }

    public static void sendUpdatedName(User user, UserLogin userLogin, String newName){
        userRepository.updateUserName(user, newName);

        userLoginRepository.updateUserName(userLogin, newName);
    }

    public static void sendUpdatedPassword(UserLogin userLogin, String newPassword){
        userLoginRepository.updatePassword(userLogin, newPassword);
    }

    public static void sendUpdatedEmail(UserLogin userLogin, String newEmail){
        userLoginRepository.updateEmail(userLogin, newEmail);
    }

    public static void sendUpdatedBirthday(User user, UserLogin userLogin, LocalDate birthDay){

        userRepository.updateBirthday(user, userLogin, birthDay);
    }

    protected static void setUserLoginRepository(UserLoginRepository userLoginRepository) {
        UserRegistry.userLoginRepository = userLoginRepository;
    }

    public static void sendUpdatedBio(User user, UserLogin userLogin, String askForBio) {
        userRepository.updateBio(user, userLogin, askForBio);
    }
}