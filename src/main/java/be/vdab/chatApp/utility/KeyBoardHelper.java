package be.vdab.chatApp.utility;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.InputMismatchException;
import java.util.Scanner;

public class KeyBoardHelper {
    private static final Scanner keyboard = new Scanner(System.in);

    public static String askForString(){
        return keyboard.nextLine();
    }

    public static int askForNumber() {
        int choice = 0;
        boolean validNumber = false;
        while (!validNumber) {
            try {
                System.out.println("Please make a choice: ");
                choice = keyboard.nextInt();
                keyboard.nextLine();
                validNumber = true;
            } catch (InputMismatchException e) {
                System.err.println("Invalid input. Please enter a number corresponding with the menu.");
            }
        }
        return choice;
    }

    public static String askForPassword() {
        String name;
        System.out.println("Please enter your password: ");
        name = keyboard.nextLine();
        return name;
    }

    public static String askForEmail() {
        String name;
        System.out.println("Please enter your email: ");
        name = keyboard.nextLine();
        return name;
    }

    public static String askForBio() {
        String name;
        System.out.println("Please enter your bio: ");
        name = keyboard.nextLine();
        return name;
    }

    public static String askForUserName() {
        String name;
        System.out.println("Please enter your username: ");
        name = keyboard.nextLine();
        return name;
    }

    public static String askForNewUserName() {
        String name;
        System.out.println("Please enter your new Username: ");
        name = keyboard.nextLine();
        return name;
    }

    public static String askForNewEmail() {
        String name;
        System.out.println("Please enter your new Email: ");
        name = keyboard.nextLine();
        return name;
    }

    public static String askForNewPassword() {
        String name;
        System.out.println("Please enter your new Password: ");
        name = keyboard.nextLine();
        return name;
    }

    public static LocalDate askForBirthday() {
        System.out.println("Please enter your Birthday in dd-mm-yyyy: ");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        String date = keyboard.nextLine();
        return LocalDate.parse(date, formatter);
    }

    public static void close() {
        keyboard.close();
    }
}
