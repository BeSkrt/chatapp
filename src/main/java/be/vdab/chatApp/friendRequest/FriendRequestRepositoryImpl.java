package be.vdab.chatApp.friendRequest;

import be.vdab.chatApp.helpers.JpaHelper;

import java.util.List;

public class FriendRequestRepositoryImpl implements FriendRequestRepository {
    private JpaHelper jpaHelper;


    public FriendRequestRepositoryImpl() {
        this(new JpaHelper());
    }

    public FriendRequestRepositoryImpl(JpaHelper jpaHelper) {
        this.jpaHelper = jpaHelper;
    }



    @Override
    public FriendRequest getFriendRequest(long id){
        return jpaHelper.find(FriendRequest.class, id);

    }

    @Override
    public void addFriendRequest(FriendRequest... friendRequests){
        jpaHelper.beginTransaction();
        for(FriendRequest friendRequest : friendRequests){
            jpaHelper.save(friendRequest);
        }
        jpaHelper.commitTransaction();
    }

    @Override
    public List<FriendRequest> getAllFriendRequest(){
        return jpaHelper.getEntityManager().createQuery("select f from FriendRequest f", FriendRequest.class).getResultList();

    }

    @Override
    public void deleteFriendRequest(FriendRequest... friendRequests){
        jpaHelper.beginTransaction();
        for(FriendRequest friendRequest : friendRequests){
            jpaHelper.remove(friendRequest);
        }
        jpaHelper.commitTransaction();
    }


    public void close(){
        jpaHelper.close();
    }
}
