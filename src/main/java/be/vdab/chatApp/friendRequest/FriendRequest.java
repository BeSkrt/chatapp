package be.vdab.chatApp.friendRequest;

import be.vdab.chatApp.user.User;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "FriendRequests")
public class FriendRequest {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id")
    private long id;
    @Column(name = "Status")
    private Status status;

    @ManyToOne
    @JoinColumn(name = "UserNameId")
    private User user1;
    @ManyToOne
    @JoinColumn(name = "FriendId")
    private User user2;

    @Column(name = "Date")
    private LocalDate date;

    public FriendRequest() {
    }

    public FriendRequest(Status status, User user1, User user2, LocalDate date) {
        this.status = status;
        this.user1 = user1;
        this.user2 = user2;
        this.date = date;
    }

    public long getId() {
        return id;
    }


    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public User getUser1() {
        return user1;
    }

    public void setUser1(User user1) {
        this.user1 = user1;
    }

    public User getUser2() {
        return user2;
    }

    public void setUser2(User user2) {
        this.user2 = user2;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "FriendRequest{" +
                "id=" + id +
                ", status=" + status +
                ", user1=" + user1 +
                ", user2=" + user2 +
                ", date=" + date +
                '}';
    }
}
