package be.vdab.chatApp.friendRequest;

import java.util.List;

public interface FriendRequestRepository extends AutoCloseable{
    FriendRequest getFriendRequest(long id);

    void addFriendRequest(FriendRequest... friendRequests);

    List<FriendRequest> getAllFriendRequest();

    void deleteFriendRequest(FriendRequest... friendRequests);
}
