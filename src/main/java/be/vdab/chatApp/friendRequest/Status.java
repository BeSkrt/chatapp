package be.vdab.chatApp.friendRequest;

public enum Status {
    ACCEPTED,
    PENDING,
    REJECTED
}
