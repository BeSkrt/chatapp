open module be.vdab.chatApp {
    requires java.sql;
    requires java.persistence;
    requires org.hibernate.orm.core;
    requires net.bytebuddy;
    requires java.xml.bind;
    requires com.fasterxml.classmate;
    requires com.sun.xml.bind;
    requires java.validation;
}